% ManyLines.t
% Draws a line, and then draw n lines parellel to of that line by changing
% the y-intercept, and color. Precondition: n % 2 == 0, n > 0.
% Jamie Langille
% Apr 26 / 2011

% what-do-I-do type question

% set � e screen graphics
setscreen ("graphics:300;300")

% draw axis
drawline (maxx div 2, 0, maxx div 2, maxy, black)
drawline (0, maxy div 2, maxx, maxy div 2, black)

% draws lines
var c : int := 0
var n : int
get n
for i : - n div 2 .. n div 2
    drawline(0, i * 10 + maxy div 2, maxx, maxy - i * 10 - maxy div 2, c)
    c := c + 1
    if (c = 256) then
	c := 0
    end if
end for

