fcn perfect (n : int) : boolean
    var c : int := 0
    for i : 1 .. n - 1
	if (n mod i) = 0 then�    c := c + i
	end if
    end for
    result (c = n)
end perfect

var n : int
loop
    put "Enter a posative integer value: " ..
    get n
    exit when (n <= 0)
    if (perfect (n)) then
	put n, " is perfect."
    else
	put n, " is imperfect."
    end if
end loop

put "Goodbye!"
delay(1000)
setscreen("invisible")
quit
