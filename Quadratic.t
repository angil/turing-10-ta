setscreen ("graphics:1000;1000")
% y=k(1/p(x-x))^2+y
% x=p*sqrt((y-y)/k)+x
var x, y, k, p : int
drawline(maxx div 2, 0, maxx div 2, maxy, black)
drawline(0, maxy div 2, maxx, maxy div 2, black)
loop
    get x
    get y
    get p
    get k
    for i : -maxx div 2 .. maxx div 2
	drawdot (i + maxx div 2, round (k * ((i - x) / p) ** 2 + y + maxy/2), brightred)
    end for
    for i : -maxy div 2 .. maxy div 2
	if ((i - y) / k > 0) then
	    drawdot (round (p * sqrt ((i - y) / k) + x ) + maxx div 2, i + maxy div 2, brightred)
	    drawdot (round (p * -sqrt ((i - y) / k) + x) + maxx div 2, i + maxy div 2, brightred)
	end if
    end for
end loop
