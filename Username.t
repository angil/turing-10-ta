% Username.t
% Creates Computer user names
% Jamie Langille
% May 3 / 2011

var f, l : string
loop
    put "Please enter your name (first last)"
    get f
    get l
    f := Str.Lower (f)
    l := Str.Lower (l)u    exit when f = "end" or l = "end"
    put "Your computer name is \"" ..
    for i : 1 .. 3
	exit when i > length (l)
	put l (i) ..
    end for
    for i : 1 .. 3
	exit when i > length (f)
	put f (i) ..
    end for
    put "\""
end loop
