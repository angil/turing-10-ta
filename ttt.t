% Tic Tac Toe
% Plays a game of Tic-Tac-Toe
% Jamie Langille
% Apr. 2 2011

% I chose 300x300 for a reason
setscreen ("graphics:300;300")

% set up data
var v : array 0 .. 8 of int
for i : lower (v) .. upper (v)
    v (i) := 0
end for
var a, r, b, x, y, button, btnUpDown : int
a := 1

% draw grid
Draw.ThickLine (100, 0, 100, 300, 3, black)
Draw.ThickLine (200, 0, 200, 300, 3, black)
Draw.ThickLine (0, 100, 300, 100, 3, black)
Draw.ThickLine (0, 200, 300, 200, 3, black)

% function for moving, drawing, and logic
function move (i : int) : boolean
    var t : boolean := false
    v (i) := a
    y := i mod 3 * 100 + 50
    x := i div 3 * 100 + 50

    % draw move
    if (a = -1) then
	Draw.Line (x - 25, y - 25, x + 25, y + 25, brightred)
	Draw.Line (x - 25, y + 25, x + 25, y - 25, brightred)
    elsif (a = 1) then
	Draw.Oval (x, y, 25, 25, brightblue)
    end if

    %check if player won

    % vertical win
    for z : i mod 3 .. 8 by 3
	exit when v (z) not= a
	if (z >= 6) then
	    Draw.ThickLine (0, y, 300, y, 3, black)
	    t := true
	end if
    end for

    % horizontal win
    for z : i div 3 * 3 .. (i div 3) * 3 + 2
	exit when v (z) not= a
	if (z mod 3 = 2) then
	    Draw.ThickLine (x, 0, x, 300, 3, black)
	    t := true
	end if
    end for

    % diagonal win
    for z : 0 .. 2
	exit when v (z * 3 + z) not= a
	if z = 2 then
	    Draw.ThickLine (0, 0, 300, 300, 3, black)
	    t := true
	end if
    end for
    for z : 0 .. 2
	exit when v (z * 3 - z + 2) not= a
	if z = 2 then
	    Draw.ThickLine (0, 300, 300, 0, 3, black)
	    t := true
	end if
    end for

    % draw
    for z : 0 .. 8
	exit when v (z) = 0
	if (z = 8) then
	    t := true
	end if
    end for

    % no one won
    result t
end move

% loop gameplay until someone wins
loop
    Mouse.ButtonWait ("down", x, y, button, btnUpDown)
    b := (y div 100) + (x div 100) * 3
    if b <= 8 and b >= 0 and v (b) = 0 then
	a := -a
	exit when (move (b))
    end if
end loop
put "GAME OVER"
