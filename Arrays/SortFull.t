% SortWow.t
% Array sorting program (Teacher / Teacher's Assistant Version)
% By: Jamie Langille
% May. 5 / 2011

setscreen ("text:max;max")

const fileName : string := "sort_large.txt"
var fileNo, cases, l : int

% Data creation (exclude)
open : fileNo, fileName, put
put : fileNo, 10000 ..
for i : 1 .. 10000
    var n : int := Rand.Int (1, 20)
    put : fileNo, "\n", n
    for j : 1 .. n
	put : fileNo, Rand.Real () * 1000 : 0 : 2, " " ..
    end for
end for
close : fileNo
quit

open : fileNo, fileName, get

proc print (n : array 1 .. * of real)
    put "{" ..
    for i : lower (n) .. upper (n) - 1
	put n (i), ", " ..
    end for
    put n (upper (n)), "}"
end print

get : fileNo, cases
var timeRunning : int
timeRunning := Time.Elapsed
for c : 1 .. cases
    get : fileNo, l
    var numbers : array 1 .. l of real
    for i : 1 .. l
	get : fileNo, numbers (i)
    end for

    % Comment the next two lines to see how fast your program runs
    put "Set before sorting: " ..
    print (numbers)

    % Modify %
    
    % Expected solution (selection sort) (exclude)
    for i : 1 .. l
	var small : int := 0
	var temp : real
	for j : i .. l
	    if (small = 0 or numbers (j) < numbers (small)) then
		small := j
	    end if
	end for
	if (small > 0) then
	    temp := numbers (i)
	    numbers (i) := numbers (small)
	    numbers (small) := temp
	else
	    break
	end if
    end for
    % Modify %

    % Comment the next two lines to see how fast your program runs
    put "Set after  sorting: " ..
    print (numbers)
end for
put "Operation completed in ", (Time.Elapsed - timeRunning), "ms."
close : fileNo
