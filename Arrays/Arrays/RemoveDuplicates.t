% "RemoveDuplicates" program
% TIK20
% May 2009
% Reads a series of names, and eliminates any duplicates

var nameCount, arrayCount : int
var name : string

put "How many names? " ..
get nameCount

put "Enter all ", nameCount, " names below (one name per line)"
% Declare the array of names
var nameList : array 1 .. nameCount of string

arrayCount := 0

% loop once for each name entered
for i : 1 .. nameCount
    % get name
    get name : *
    
    % Search the array of names to see whether the 
    % name has already been entered before
    var found : boolean := false
    for j : 1 .. arrayCount
	if nameList(j) = name then
	    % the name is a duplicate of a name already entered before
	    found := true    
	    exit
	end if
    end for
    
    % if the name was not already entered before,
    % then add the name into the next "slot" of the names array
    if not found then
	arrayCount := arrayCount + 1
	nameList(arrayCount) := name
    end if
end for

% Display the list of names in the names array, 
%  any duplicates
put "Here are the names without any duplicates:"
for i : 1 .. arrayCount
    put nameList(i)
end for
