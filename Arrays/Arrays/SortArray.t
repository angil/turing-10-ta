% The "SortArray" program
% Reads a list of positive integers and
% sorts it into ascending order
% Name: TIK20
% Date: May 2009

var count : int

put "How many integers in list? " ..
get count

% Read list into an array
var list : array 1 .. count of int
put "Enter the numbers in the list"
for i : 1 .. count
    get list(i)
end for

% Declare a second array, which will hold the numbers in sorted order
var sortList : array 1 .. count of int

for i : 1 .. count
    % Find smallest remaining in list
    var smallest := 2147483647  % largest possible int in turing
    var where : int
    for j : 1 .. count
	if list (j) < smallest then
	    smallest := list(j)
	    where := j
	end if
    end for
    
    % Store smallest in sortList array
    sortList(i) := smallest
    
    % Replace smallest in array list with largest int possible in turing 
    list(where) := 2147483647
end for

% Display sorted list
put "Here are the numbers in ascending order:"
for i : 1 .. count
    put sortList(i)
end for
