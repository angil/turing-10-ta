% Bubble Sort
% Gets a bunch of integers into an array, and then sorts them.
% Name: ICS2O
% Date: � y 2009

var temp, count : int

put "How many integers? "..
get count

var num : array 1 .. count of int

for i : 1 .. count
    put "Enter #", i, " >" ..
    get num(i)
end for    

for decreasing i : (count-1)..1 
    for j : 1 .. i
	if num(j) > num(j+1) then
	    temp := num(j)
	    num(j) := num(j+1)
	    num(j+1) := temp
	end if
    end for
end for    

put "The integers in ascending order are:"
for i: 1..count
    put num(i)
end for     

