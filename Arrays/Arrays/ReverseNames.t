% The "ReverseNames" program
% Reads a list of names and outputs in reverse
% Demonstrates setting the array size using a variable
% Name: TIK20
% Date: September 2007

var howMany : int
% Ask the user how many names there are and store in variable howMany
put "How many names are there? " ..
get howMany

% Declare an array, name, of size "howMany", to store a list of strings
var name : array 1 .. howMany of string 
put "Enter ", howMany, " names, one to a line"

% Get names from user and store in array
for i : 1 .. howMany
    get name(i) 
end for
put "Here are the names in reverse order"

% Output names in reverse order
for decreasing i : howMany .. 1
    put name(i)
end for
