% Program: TenNumbersWithoutArray
% Name: TIK20 
% Date: November 2007
% Gets 10 numbers from the user and then redisplays them.
% Look at program ArrayTenNumbers to see how shorter this is using an array 

var num1, num2, num3, num4, num5, num6, num7, num8, num9, num10 : int

put "Enter 10 integers"

% Get 10 numbers, storing each number into a different variable
get num1
get num2 
get num3
get num4
get num5
get num6
get num7
get num8
get num9
get num10

% Display all the numbers entered
put ""
put "The numbers you entered are:"
put num1
put num2 
put num3
put num4
put num5
put num6
put num7
put num8
put num9
put num10 

% Re-display the numbers in reverse order
put ""
put "The numbers in reverse order are:"
put num10
put num9
put num8
put num7
put num6
put num5
put num4
put num3
put num2
put num1 
