% ArrayTenNumbers
% Name: TIK20 
% Date: November 2007
% Gets 10 numbers from the user (int� an anrray), and then redisplays them 

var num : array 1 .. 10 of int

put "Enter 10 integers"

% Get 10 numbers, storing each number into an array of 10 integers
for i : 1 .. 10
    get num(i) 
end for 

% Display all the numbers entered
put ""
put "The numbers you entered are:"
for i : 1 .. 10
    put num(i)
end for  

% Re-display the numbers in reverse order
put ""
put "The numbers in reverse order are:"
for decreasing i : 10 .. 1
    put num(i)
end for
