% The "ArrayAverage" program
% Reads a list of marks and computes their average.
% Name: TIK20 
% Date: November 2007

var count, sum: int
sum := 0
var average: real

% Ask for and get the number of students
put "How many students in class? " ..
get count

% Create an array for the exact number of students
var mark : array 1 .. count of int

% Prompt user to enter marks
put ""
put "Enter the marks for the students:"

% Store each mark entered into a different element of the array
for i : 1 .. count
    put "Mark #", i
    get mark(i)
end for

% Display all the marks entered
put ""
put "You entered: "..
for i : 1 .. count
    put mark(i), " "..
end for

% Add all marks in the array and find the average
for i : 1 .. count
    sum := sum + mark(i)
end for
average := sum / count

% Output the average to two decimal places
put " "
put "Average mark is ", average:0:2





