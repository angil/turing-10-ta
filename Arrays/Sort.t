% Sort.t
% Array sorting program
% By: Jamie Langille
% May. 5 / 2011
% Modified by [your name here] on [today's date]

setscreen ("text:max;max")

const fileName : string := "sort.txt"
var fileNo, cases, l : int
open : fileNo, fileName, get
proc print (n : array 1 .. * of real)
    put "{" ..
    for i : lower (n) .. upper (n) - 1
	put n (i), ", " ..
    end for
    put n (upper (n)), "}"
end print
get : fileNo, cases
var timeRunning : int
timeRunning := Time.Elapsed
for c : 1 .. cases
    get : fileNo, l
    var numbers : array 1 .. l of real
    for i : 1 .. l
	get : fileNo, numbers (i)
    end for

    % Comment the next two lines to see how fast your program runs
    put "Set before sorting: " ..
    print (numbers)

    % Modify % Add your code here to sort the array 'numbers'
    
    % Modify %

    % Comment the next two lines to see how fast your program runs
    put "Set after  sorting: " ..
    print (numbers)
end for
put "Operation completed in ", (Time.Elapsed - timeRunning), "ms."
close : fileNo
