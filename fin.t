var fileName : string := "Primes.t"
var fileNo : int
var input : string
open : fileNo, fileName, read
loop
    exit when eof(fileNo)
    read : fileNo, input
    put input ..
end loop
close : fileNo
