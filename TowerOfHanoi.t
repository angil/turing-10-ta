% TowerOfHanoi.t
% Draws a single <i>Tower of Hanoi</i> with n disks,
% given that n <= 600, n e R.
% Jamie Langille
% Apr. 26 / 2011

% (fill-in-the-code or program) type question

% set screen
setscreen ("graphics:300;300")

% set variables
var n, h, dw, w, c : int
c := 1
% Get the number of disks
get n

% Calculate the height of each disk
h := round (maxy / n)

% Calculate how the next disk is narrower than the previous
w := 0
dw := round ((maxx / n) / 2)

% Loop through n disks, drawing each disk at
% its corresponding position
for i : 1 .. n
    drawbox (dw * (i - 1), h * (i - 1), maxx - dw * (i - 1), h * i, c)
    c := c + 1
    if (c = 256) then
	c := 1
    end if
end for

