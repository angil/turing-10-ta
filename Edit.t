% Edit.t
% Allows a user to manipulate a tring
% Jamie Langille
% May 3 / 2011

var input, letter, replace : string
var n : int
loop
    put "Menu"
    put "1. Count a letter"
    put "2. Eliminate letter"
    put "3. Replace a letter"
    put "4. Exit"
    put ""
    put "Enter an option: " ..
    get input
    loop
	exit when strintok (input) and strint (input) > 0 and strint (input) < 5
	put "Enter an option: " ..
	get input
    end loop
    exit when strint (input) = 4
    case strint (input) of
	label 1 :
	    put "Please enter a word: " ..
	    get input
	    put "Please enter a letter: " ..
	    get letter
	    n := 0
	    for i : 1 .. length (input)
		if (index (input (i .. *), letter) = 1) then
		    n := n + 1;
		end if
	    end for
	    put "Letter ", letter, " appears in ", input, " ", ceil(n/length(letter)), " times."
	label 2 :
	    put "Please enter a word: " ..
	    get input
	    put "Please enter a letter: " ..
	    get letter
	    put input, " without ", letter, " is: " ..
	    for i : 1 .. length (input)
		if (index(input (i .. *), letter) not= 1) then
		    put input (i) ..
		end if
	    end for
	    put ""
	label 3 :
	    put "Please enter a word: " ..
	    get input
	    put "Please enter the letter to replace: " ..
	    get letter
	    put "Please enter the letter to replace with: " ..
	    get replace
	    put "Your new word is: " ..
	    for i : 1 .. length (input)
		if (index (input (i .. *), letter) not= 1) then
		    put input(i) ..
		elsif (index (input (i .. *), letter) = 1) then
		    put replace ..
		end if
	    end for
	    put ""
    end case
    put "Press any key to continue..." ..
    Input.Pause
    cls
end loop
delay(500)
quit
