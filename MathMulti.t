var a, b, c, ans : int
c := 0
for i : 0 .. 4
    randint(a, 0, 100)
    randint(b, 0, 100)
    put a, " x�, b, " = " ..
    get ans
    if (ans = a*b) then
	put "Correct!"
	c := c + 1
    else
	put "Incorrect."
    end if
    
end for
put c, " / 5 correct: ", (c/5)*100, "%."
put Time.Elapsed/1000, " seconds"
