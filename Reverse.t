% Reverse.t
% Outputs words in reverse
% Jamie Langille
% May 3 / 2011

var input : string
put "Enter a word, or 'end' to exit."
loop
    get input
    exit when input = "end"
    put "The reverse word is " ..
 | for decreasing i : length(input) .. 1
	put input(i) ..
    end for
    put ""
end loop
