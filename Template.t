% Template.t
% A program for random testing, which has a decent header as an example
% By: Jamie Langille
% Date: 27/02/2011

% 5 test cases
for rep : 1 .. 5

    % Get M, N, P, Q from the user
    var m, n, p, q : int
    get m
    get n
    get p
    get q

    % Calculate W, the width of the entire frame
    var w : int := m + 2 * p + 2 * q

    % Print (only #)
    for i : 1 .. q
	for j : 1 .. w
	    put '#' ..
	end for
	put ""
    end for

    % Print (only # and +)
    for i : 1 .. p
	for j : 1 .. q
	    put '#' ..
	end for
	for j : 1 .. w - 2 * q
	    put '+' ..
	end for
	for j : 1 .. q
	    put '#' ..
	end for
	put ""
    end for

    % Print (#, + and .)
    for i : 1 .. n
	for j : 1 .. q
	    put '#' ..
	end for

	for j : 1 .. p
	    put '+' ..
	end for

	for j : 1 .. m
	    put '.' ..
	end for

	for j : 1 .. p
	    put '+' ..
	end for

	for j : 1 .. q
	    put '#' ..
	end for
	put ""
    end for

    % Print (only # and +)
    for i : 1 .. p
	for j : 1 .. q
	    put '#' ..
	end for
	for j : 1 .. w - 2 * q
	    put '+' ..
	end for
	for j : 1 .. q
	    put '#' ..
	end for
	put ""
    end for

    % Print (only #)
    for i : 1 .. q
	for j : 1 .. w
	    put '#' ..
	end for
	put ""
    end for
end for
