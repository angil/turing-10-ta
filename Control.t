% Control.t
% Examples of control structures
% Jamie Langille
% Mar. 16/2011

var a, b, c, d : int

% The 'if' statement is the conditional statement
% if [condition] then [...] end if
% [condition] is anything that can be true or false (boolean)
% [...] is any number of statements or lines of code 'within' the if

% comparisons
% > < <= >= = not=
a := 2
b := 5
if (a <= b) then
    put "a <= b"
end if
put ""

% compound comaprisons: if...elsif...else
c := 0
if (a < b) then
    put "a < b"
elsif (b < c) then
    put "b < c"
elsif (a + b = c) then
    put "a < c, b < c, c = b + c"
else
    put "c < b < a"
end if
put ""

% nested comparisons (ifs within ifs)
% notice this can be hard to follow if you refuse to indent your work
if (a < b) then if (b < c) then put "a < b < c" else
put "a < b, c <= b"
end if elsif (b < c) then if (c < a) then
put "b < c < a"
else put "b < c, a <= c"
end if elsif (c < a) then if (a < b) then put "c < a < b"
else put "c < a, b <= a"
end if else put "What's going on?"
end if
put ""

% the loop...(exit when [condition])...end loop for wrapping statements in repeated loops
% exit when [condition] to exit the loop
a := 0
loop
    exit when (a = 7)
    a += 1;
    put a
end loop
put ""

% nested loops and ifs
a := 0
b := 1
c := 0
loop

    % what is this doing? what is 'c' for?
    c := b
    b := a + b
    a := c
    
    % even number
    if (a mod 2 = 0) then
	put a, " is even"
    else
	put a, " is odd"
    end if
    
    % careful...
    exit when (a >= 20)
end loop
put ""


% The 'for' loop
% for (decreasing) [var] : [start] .. [end] (by [increment])
a := 0
for decreasing i : 10 .. 0 by 2
    put i, " " ..
    a += i
end for
put "\t" ..
put a
