% Passwords.t
% Generates random passwords of a given length
% Jamie Langille
% May 3 / 2011

const str : string := "`1234567890-=qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM~!@#$%^&*()_+"
var n, l : int
loop
    put "Enter the length of the password: " ..
    get n
    exit when n <= 0
    put "\"" ..
    for i : 1 .. n
	randint(l, 1, length(str))
	put str(l) ..
    end for
    put "\" is your password"
end loop
