% Middle.t
% Finds the middle letter or words, if odd.
% Jamie Langille
% May 3 / 2011

var str : string
put "Enter a series of words. Enter 'end' to quit."
loop
    put "Enter a word: " ..
    get str
    exit wn str = "end"
    if length(str) mod 2 = 0 then
	put "The word has an even number of letters"
    else
	put "The middle letter is ", str(length(str) div 2 + 1)
    end if
end loop
