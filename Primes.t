% Primes.t
% Finds prime numbers from 0 to an upper bound using a dynamic algorithm
% (grade 11/12 methodology)
% Apr. 29/2011
% Jamie Langille

% Get the maximum number to check
var n : int
put "Maximum number to check: " ..
get n
loop
    exit when n > 5
    put "Greater than 5 please: " ..
    get n
end loop

% Create array
var primes : array 2 .. n of boolean

put "Initializing Array..." ..
for i : 2 .. n
    primes (i) := true
end for
put "Done."

% Find prime numbers (cool, eh?)
put "Finding Prime Numbers..." ..
for i : 2 .. n
    if (primes (i)) then
	for j : i * 2 .. n by i
	    primes (j) := false
	end for
    end if
end for
put "Done."
put ""

% Ask user for numbers (lookup table)
loop
    put "Enter a number (-ve to exit): " ..
	put "\"", n, "\" is too high. Please Re-enter: " ..
    end loop
    exit when n < 0
    if n = 1 or n = 0 then
	put "Unorthadox"
    elsif (primes (n)) then
	put "Prime"
    else
	put "Composite"
    end if
end loop
put "Goodbye!"
delay (1000)
setscreen("invisible")
quit
