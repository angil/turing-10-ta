% Matrix.t
% Generates a matrix screen, close to the movies
% May 4 2011
% Jamie Langille

% Set the green to preferred mode (not quite fullscreen, whatever)
setscreen ("graphics:max;max,nobuttonbar")

% Class to define a 'stream' of characters
class TextStream

    % Make evertyhing visible
    export initialize, draw, y, x

    % Data: x and y-coordinates of the next character to be drawn
    var x, y : int

    % Initialize: gives initial values to x and y
    proc initialize (a : int, b : int)
	x := a
	y := b
    end initialize

    % Draws a letter, then adds 20 to 'y' for the next letter
    proc draw ()
	Draw.Text ("" + cheat (char, Rand.Int (1, 127)), x, y, defFontID, brightgreen)
	% Eraser
	drawfillbox (x, y + 120, x + 20, y + 140, black)
	y := y - 15
    end draw
end TextStream

% How many streams
var n : int := maxy

% The streams themselves
var a : array 1 .. n of pointer to TextStream

% Totally random initialization
for i : 1 .. n
    new TextStream, a (i)
    a (i) -> initialize (Rand.Int (0, maxx), Rand.Int (0, maxy))
end for

% Background
drawfillbox (0, 0, maxx, maxy, black)

% Continuously draw / modify all streams
loop
    for i : 1 .. n
	a (i) -> draw ()
	if (a (i) -> y < -140) then
	    a (i) -> initialize (Rand.Int (0, maxx), Rand.Int (maxy div 4, maxy))
	end if
    end for
    delay (5)
end loop
