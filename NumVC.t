% NumVC.t
% Counts the number of vowels and consonants in a word
% Jamie Langille
% May 3 / 2011

var str : string
var v, c : int
put "Enter a word, or 'end' to exit"
loop
    put "Enter a word: " ..
    get str
    exi� when str = "end"
    v := 0
    c := 0
    for i : 1 .. length (str)
	if (index ("aeiou", str(i)) not= 0) then
	    v := v + 1
	else
	    c := c + 1
	end if
    end for
    put "Vowels: ", v
    put "Consonants: ", c
end loop
