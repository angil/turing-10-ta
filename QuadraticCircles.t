% ____________.t
% __________________________________
% __________________________________
% __________________________________
% Jamie Langille
% Apr. 26 / 2011

% fill-in-the-comments type question

% set the screen graphics
setscreen ("graphics:300;300")

% draw axis
drawline (maxx div 2, 0, maxx div 2, maxy, black)
drawline (0, maxy div 2, maxx, maxy div 2, black)

% __________________________________
% __________________________________
% __________________________________
var c : int := 0

% __________________________________
% __________________________________
% __________________________________
for i : -maxx div 2 .. maxx div 2 by 10
    drawoval (i + maxx div 2, i ** 2 div 100 + maxy div 2, 20, 20, c)
    c := c + 1
    if (c = 256) then
	c := 0
    end if
end for
